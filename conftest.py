import pytest
from random import choice
from string import ascii_letters


@pytest.fixture
def random_text():
    return ''.join(choice(ascii_letters) for i in range(12))
